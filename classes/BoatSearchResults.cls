public class BoatSearchResults {

    @AuraEnabled
    public static List<Boat__c> getAccounts() {
        
        return ([SELECT Id, Name FROM Boat__c]);
    }
    
}