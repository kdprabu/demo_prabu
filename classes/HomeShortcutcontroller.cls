public class HomeShortcutcontroller{
    
    @AuraEnabled
    public static List<Shortcuturl__c> getshortCuts()
    {
        //List<Shortcuturl__c> shortCuts1 = Shortcuturl__c.getall().values();
        List<Shortcuturl__c> shortCuts = [select Name,URL__c from Shortcuturl__c ORDER BY Order__c ASC NULLS LAST];
        return shortCuts;
    }

}